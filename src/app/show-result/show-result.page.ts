import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage implements OnInit {

  workTime;
  timeAvailable;
  highSchool;
  education;
  graduated;
  university;
  convicted;



  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
  ) {
    let content = this.clientInfo.clientInfo[0].content;
    this.workTime = content[0][1];
    this.timeAvailable = content[1][1];
    this.highSchool = content[2][1];
    this.education = content[3][1];
    this.graduated = content[4][1];
    this.university = content[5][1];
    this.convicted = content[6][1];

   }

  ngOnInit() {
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }

    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/application']);
  }

}
