import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckResultPage } from './check-result.page';

describe('CheckResultPage', () => {
  let component: CheckResultPage;
  let fixture: ComponentFixture<CheckResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckResultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
