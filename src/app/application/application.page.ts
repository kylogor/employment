import { Component, OnInit } from '@angular/core';
import { ChangeDataService } from 'src/services/change-data.service';
import { Listener,NetworkType } from 'tsjs-xpx-chain-sdk';
import { LoginRequestMessage, VerifytoLogin, CredentialRequestMessage, Credentials ,ApiNode} from 'siriusid-sdk';
import { Router } from '@angular/router';
import { ClientInfoService } from 'src/services/client-info.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  currentNode = "demo-sc-api-1.ssi.xpxsirius.io";
  subscribe;
  listener: Listener;
  connect = false;
  qrImg;
  showImg = false;
  qrCredential;

  linkLogin;
  linkCredential;
  
  workTime;
  timeAvailable;
  highSchool;
  education;
  graduated;
  university;
  convicted;
  aggree = false;

  warning = false;
  warning2 = false;
  question = false;
  flag = true;
  step0 = true;
  step1 = false;
  step2 = false;
  step3 = false;
  step4 = false;
  credentialChosen = ["proximaxId","bankID"];
  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    private clientInfo:ClientInfoService
  ) { 
    ApiNode.apiNode = "https://"+this.currentNode;
    ApiNode.networkType=NetworkType.PRIVATE_TEST;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if(this.flag){

      this.step0 = false;
      this.question = true;
      this.flag = false;
    } else {
      this.question = false;
      this.step0 = true;
    }
  }

  ngOnInit() {
  }

  async loginRequestAndVerify(){
    this.connect = false;
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp,this.credentialChosen);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.qrImg = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && !this.connect && listenerAsAny.webSocket.readyState !== 0){
        this.warning2 = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        
        this.connection(sessionToken);
        
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
        
      }
      else if(this.connect){
        clearInterval(interval);
        
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    console.log("question is:",this.question);
    this.listener.open().then(() => {
      this.warning2 = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        VerifytoLogin.verify(transaction, sessionToken,["proximaxId"],this.changeDataService.privateKeydApp).then(
          verify=>{
            if (verify){
              this.connect = true;
              console.log("Transaction matched");
              this.changeDataService.addressSiriusid = transaction.signer.address.plain();
              this.subscribe.unsubscribe();
              this.step1 = false;
              this.step2 = true;
            }
            else console.log("Transaction not match");  
          }
        )
               
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
  async continue(){
    if (this.workTime && this.timeAvailable && this.highSchool && this.education 
      && this.graduated && this.university && this.convicted && this.aggree){
        let content = new Map<string,string>([
          ['Employment type', this.workTime],
          ['Available time to work', this.timeAvailable],
          ['High school', this.highSchool],
          ["Your Education", this.education],
          ["Graduated", this.graduated],
          ["College - University", this.university],
          ["Have you ever been convicted or a crime", this.convicted]
        ])

        const credential = Credentials.create(
          'employmentID',
          'EmployID.com',
          'Employment IDentification',
          '/assets/employment.svg',
          [],
          content,
          "",
          Credentials.authCreate(content,this.changeDataService.privateKeydApp)

        );
        const msg = CredentialRequestMessage.create(credential);

        this.qrCredential = await msg.generateQR();
        this.linkCredential = await msg.universalLink();

        this.warning = false;
        this.step2 = false;
        this.step3 = true;
      }
    else{ 
      this.warning = true;
    }
  }

  begin(){
    this.question = false;
    this.step0 = true;
    this.step1 = false;
  }
  start(){
    this.step0 = false;
    this.step1 = true;
    this.loginRequestAndVerify();
  }
  
  finish(){
    this.step3 = false;
    this.step4 = true;
  }

  applyForm(){

    this.connect = true;
    // this.subscribe.unsubscribe();
    this.step0 = true;
    this.step1 = false;
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.router.navigate(['/application']);
  }
  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }

    this.connect = true;
    // this.subscribe.unsubscribe();
    this.step0 = true;
    this.step1 = false;
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    
    this.router.navigate(['/check-result']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
