import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {

  pubickeySiriusid =''; 
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  
  /**
   * For TEST_NET
   */
  publicKeydApp = '1130DC883896DB996D59C2CBCEE55F845E95AC70B1F522FC44A5705B160372A0'; 
  privateKeydApp = '7D94BB4D0C0B90591E7F05F57AE1216892B55EAE4F29B6B00B26F1356F87A96A';
  addressdApp: Address;
  apiNode = "";
  ws = 'wss://' + this.apiNode; //websocket
  //ws = "wss://bctestnet3.xpxsirius.io";
  
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
  updateWebsocket(){
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() { 
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.PRIVATE_TEST);
  }
}
