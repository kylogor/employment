import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientInfoService {

  client_name:string = "";
  client_gender:string = "";
  client_country:string = "";
  client_zipcode:string = "";
  clientInfo :any;

  haveCredential = false;
  lightVerification = false;
  
  constructor() { }
}
